// SPDX-License-Identifier: MIT

pragma solidity ^0.8.0;

import './ERC721Auctionable.sol';

/**
 * @title PustotaCore - version 0
 * PustotaCore - root contract for our plaform (and token registry).
 */
contract PustotaCoreV0 is ERC721Auctionable {
    constructor()
        ERC721Auctionable(
            'Pustota',
            'PST',
            payable(TREASURY_ACCOUNT_ID) // Treasury account
        )
    {}

    function baseTokenURI() public pure override returns (string memory) {
        return 'https://pustota.io/api/metadata';
    }

    function contractURI() public pure returns (string memory) {
        return 'https://pustota.io/api/metadata/contract';
    }
}
