// SPDX-License-Identifier: MIT

pragma solidity ^0.8.0;

import './PustotaERC721.sol';
import './PustotaERC721Enumerable.sol';
import './openzeppelin/access/Ownable.sol';
import './openzeppelin/utils/math/SafeMath.sol';
import './openzeppelin/utils/Strings.sol';

/**
 * @title ERC721Auctionable
 * ERC721Auctionable - ERC721 with auction and "complex" rewards rules
 */
abstract contract ERC721Auctionable is PustotaERC721Enumerable, Ownable {
    using Strings for string;
    using SafeMath for uint256;

    // We need three values for the auctionStat
    enum AuctionState { ReadyToStart, Started, Ended }

    // Address that's used to collect platform fees
    address payable private platformAccount;

    // Withdrawal balances consisting of not winning bet
    mapping(address => uint256) public pendingReturns;

    // Offers to buy tokens tokenId -> (offerer -> amount)
    mapping(uint256 => mapping(address => uint256)) public offers;

    // Offers gallerists to buy tokens tokenId -> (offerer -> gallerist)
    mapping(uint256 => mapping(address => address)) public offersGallerists;

    // No new auctions when it's true
    bool public isPaused;

    // Time (unix in seconds) when isPaused had become true
    uint256 public pausedFrom;

    // If price is positive value then any user can buy the token
    mapping(uint256 => uint256) public prices;

    /* Contract parameters which can be changed when no auction is running */

    // Limit for auction duration (in seconds)
    uint32 public maxAuctionDuration;

    // Fee of the author (in microunits)
    uint24 public authorFee;

    // Fee of the platform (in microunits) collected from each offer to buy token
    uint24 public offerFee;

    // Fee of the platform (in microunits) collected from each simple sale
    uint24 public saleFee;

    // Fee of the platform (in microunits) collected from each bet
    uint24 public bidFee;

    // Fee of withdrawall action (in microunits)
    uint24 public withdrawalFee;

    // Fee of the platform (in microunits) collected from all first sells
    uint24 public firstSellFee;

    // Fee of the platform (in microunits) collected from all secondary sells
    uint24 public secondarySellFee;

    // Fee of the gallerist (in microunits)
    uint24 public galleristFee;

    // Minimal incremention of highest bid during auction (in microunits of startingPrice)
    uint24 public minBidIncrement;

    /* End of contract parameters which can be changed when no auction is running */

    // Author is the person who has created tokenized digital data
    mapping(uint256 => address) public author;

    // Highest price of token among all auctions
    mapping(uint256 => uint256) public tokenHighestPrice;

    // Beneficiary takes some share after an auction has been ended
    mapping(uint256 => address) public beneficiary;
    mapping(uint256 => uint24) public beneficiaryShare;

    // Starting price for a token
    mapping(uint256 => uint256) public startingPrice;

    // The end time of the auction and prolongation time.
    // Both are either absolute unix timestamps (seconds since 1970-01-01) or time periods in seconds.
    mapping(uint256 => uint256) public auctionStartTime;
    mapping(uint256 => uint256) public auctionEndTime;
    mapping(uint256 => uint24) public timeStep;

    // Highest bid and corresponding bidder and gallerist addresses
    mapping(uint256 => uint256) public highestBid;
    mapping(uint256 => address) public highestBidder;
    mapping(uint256 => address) public highestGallerist;

    // Current state of auctions
    mapping(uint256 => AuctionState) public auctionState;

    // Equals true if auction is run second time or more
    mapping(uint256 => bool) public notFirstSell;

    // Events
    event NewPrice(uint256 indexed tokenId, uint256 oldPrice, uint256 newPrice);
    event HighestBidIncreased(
        uint256 indexed tokenId,
        address bidder,
        uint256 amount,
        address gallerist
    );
    event AuctionStarted(
        uint256 indexed tokenId,
        address creator,
        uint256 auctionEndTime,
        uint256 startingPrice
    );
    event AuctionEndTimeIncreased(uint256 indexed tokenId, uint256 endTime);
    event AuctionEnded(uint256 indexed tokenId, address winner, uint256 amount);
    event NewPriceOffer(uint256 indexed tokenId, address offerer, uint256 price);
    event OfferAccepted(uint256 indexed tokenId, address offerer, uint256 price);

    // Mint is Transfered from null address

    // Constructor
    constructor(
        string memory _name,
        string memory _symbol,
        address payable _platformAccount
    ) PustotaERC721(_name, _symbol) {
        platformAccount = _platformAccount;
        maxAuctionDuration = 1 weeks;
        withdrawalFee = 5000; // 0.5 %
        offerFee = 30000; // 3%
        saleFee = 30000; // 3%
        authorFee = 100000; // 10%
        bidFee = 30000; // 3%
        firstSellFee = 50000; // 5%
        secondarySellFee = 0; // 0%
        galleristFee = 100000; // 10%
        minBidIncrement = 10000; // 1%
    }

    /**
     * @dev Returns the address of the current owner.
     */
    function getPlatformAccount() public view returns (address) {
        return platformAccount;
    }

    /**
     * @dev Change address for platform fees
     * @param _platformAccount new account address of the platform
     */
    function setPlatformAccount(address payable _platformAccount) public onlyOwner {
        platformAccount = _platformAccount;
    }

    /**
     * @dev Set isPaused as true
     */
    function disableAuctionCreation() public onlyOwner {
        require(!isPaused);
        isPaused = true;
        pausedFrom = block.timestamp;
    }

    /**
     * @dev Set isPaused as true
     */
    function enableAuctionCreation() public onlyOwner {
        isPaused = false;
    }

    /**
     * @dev Change contract parameters.
     * Contract owner can call this only the moment when all auction can be ended plus 1 day
     */
    function setContractParameters(
        uint32 _maxAuctionDuration,
        uint24 _authorFee,
        uint24 _offerFee,
        uint24 _saleFee,
        uint24 _bidFee,
        uint24 _withdrawalFee,
        uint24 _firstSellFee,
        uint24 _secondarySellFee,
        uint24 _galleristFee,
        uint24 _minBidIncrement
    ) public onlyOwner {
        // Check that auction creation is now allowed
        require(isPaused);

        // It must pass (maxAuctionDuration + 1 day) after the creation of auctions was disabled
        require(block.timestamp > pausedFrom + maxAuctionDuration + (1 days));

        // Update variables
        maxAuctionDuration = _maxAuctionDuration;
        authorFee = _authorFee;
        offerFee = _offerFee;
        saleFee = _saleFee;
        bidFee = _bidFee;
        withdrawalFee = _withdrawalFee;
        firstSellFee = _firstSellFee;
        secondarySellFee = _secondarySellFee;
        galleristFee = _galleristFee;
        minBidIncrement = _minBidIncrement;
    }

    /**
     * @dev Mints a token to an address with a tokenURI.
     * @param _to address of the token new owner
     */
    function mintTo(
        address _to,
        address _authorId,
        uint256 _tokenId
    ) public {
        author[_tokenId] = _authorId;
        _mint(_to, _tokenId);
    }

    /**
     * @dev Run new auction for the token.
     * Only token owner or approved address can start auction.
     */
    function startAuction(
        uint256 _tokenId,
        uint256 _startingPrice,
        uint256 _auctionEndTime,
        uint24 _timeStep,
        address _beneficiary,
        uint24 _beneficiaryShare
    ) public {
        // Check call permission
        require(
            _isApprovedOrOwner(_msgSender(), _tokenId),
            'ERC721Auctionable: startAuction caller is not owner nor approved'
        );

        // Check that auction creation is allowed
        require(!isPaused);

        // Check that auction isn't running now
        require(
            auctionState[_tokenId] == AuctionState.ReadyToStart,
            'ERC721Auctionable: auction cannot be run'
        );

        // Check that _auctionEndTime is correct
        require(
            _auctionEndTime > block.timestamp,
            'ERC721Auctionable: auctionEndTime is less than current time'
        );
        require(
            _auctionEndTime - block.timestamp < maxAuctionDuration,
            'ERC721Auctionable: auctionEndTime exceeds the allowed limit'
        );

        // Check that total share (in microunits) is not greater that 1 000 000
        uint24 _sellFee;
        if (!notFirstSell[_tokenId]) {
            _sellFee = firstSellFee;
        } else {
            _sellFee = secondarySellFee;
        }
        require(
            _sellFee + galleristFee + _beneficiaryShare + authorFee <= 1000000,
            'ERC721Auctionable: total share exceeds 100%'
        );

        // Start auction
        auctionState[_tokenId] = AuctionState.Started;
        startingPrice[_tokenId] = _startingPrice;
        auctionStartTime[_tokenId] = block.timestamp;
        auctionEndTime[_tokenId] = _auctionEndTime;
        timeStep[_tokenId] = _timeStep;
        beneficiary[_tokenId] = _beneficiary;
        beneficiaryShare[_tokenId] = _beneficiaryShare;

        // Reset auction variable
        highestBid[_tokenId] = 0;
        highestBidder[_tokenId] = address(0);
        highestGallerist[_tokenId] = address(0);

        // Fire the event
        emit AuctionStarted(_tokenId, msg.sender, _auctionEndTime, _startingPrice);
    }

    /**
     * @dev Bid on the auction with the value sent together with this transaction.
     * The value will only be refunded if the auction is not won.
     */
    function bid(
        uint256 _tokenId,
        address _gallerist,
        uint256 _bid
    ) public payable {
        // Revert the call if the bidding period is over.
        require(
            block.timestamp <= auctionEndTime[_tokenId],
            'ERC721Auctionable: Auction end time was passed.'
        );

        // Bid must be higher or equal to startingPrice
        require(_bid >= startingPrice[_tokenId]);

        // Revert the call if the bid is not higher the highest bid
        uint256 minBidIncrementWei = (minBidIncrement * startingPrice[_tokenId]) / 1000000;
        minBidIncrementWei -= 1;
        require(
            _bid > highestBid[_tokenId] + minBidIncrementWei,
            'ERC721Auctionable: There is already a higher bid or bid increment is too small.'
        );

        // Revert if msg.value is not enough
        uint256 bidWithFee = _bid + (_bid * bidFee) / 1000000;
        if (msg.value < bidWithFee) {
            require(
                bidWithFee - msg.value <= pendingReturns[msg.sender],
                'ERC721Auctionable: Not enough ether to bid and pay fee'
            );

            // If ether value is less than bid with fee then we take lacking part from pendingReturns
            pendingReturns[msg.sender] -= bidWithFee - msg.value;
        }

        // Update pending returns of previous bidder if needed
        uint256 outbidReturnAmount = 0;
        if (highestBid[_tokenId] != 0) {
            // Sending back the money by simply using
            // highestBidder.send(highestBid) is a security risk
            // because it could execute an untrusted contract.
            // It is always safer to let the recipients
            // withdraw their money themselves.
            outbidReturnAmount = highestBid[_tokenId] + (highestBid[_tokenId] * bidFee) / 1000000;
            pendingReturns[highestBidder[_tokenId]] += outbidReturnAmount;
        }

        // Update highest bidder, highest bid and highestGallerist
        highestBidder[_tokenId] = msg.sender;
        highestBid[_tokenId] = _bid;
        highestGallerist[_tokenId] = _gallerist;

        // Send bid fee
        pendingReturns[owner()] += bidWithFee - _bid;

        // If sended amount was higher than bidWithFee then increase pendingReturns
        if (msg.value > bidWithFee) {
            pendingReturns[msg.sender] += msg.value - bidWithFee;
        }

        // Increase auction time if needed
        if (block.timestamp + timeStep[_tokenId] >= auctionEndTime[_tokenId]) {
            if (timeStep[_tokenId] > 0) {
                // Check that auction duration doesn't exceed maxAuctionDuration
                uint256 newAuctionEndTime = auctionEndTime[_tokenId] + timeStep[_tokenId];
                if (newAuctionEndTime < auctionStartTime[_tokenId] + maxAuctionDuration) {
                    auctionEndTime[_tokenId] = newAuctionEndTime;

                    // Fire the event
                    emit AuctionEndTimeIncreased(_tokenId, auctionEndTime[_tokenId]);
                }
            }
        }

        // Fire the event
        emit HighestBidIncreased(_tokenId, msg.sender, highestBid[_tokenId], _gallerist);
    }

    /**
     * @dev Users can withdraw their means in contract.
     */
    function withdraw() public returns (bool) {
        uint256 amount = pendingReturns[msg.sender];
        if (amount > 0) {
            // First set pendingReturn to zero
            pendingReturns[msg.sender] = 0;

            // Compute withdrawal fee value
            uint256 withdrawalFeeAmount = (withdrawalFee * amount) / 1000000;
            amount -= withdrawalFeeAmount;

            // Payments
            if (!payable(msg.sender).send(amount)) {
                // No need to call throw here, just reset the amount owing
                pendingReturns[msg.sender] = amount + withdrawalFeeAmount;
                return false;
            } else {
                pendingReturns[owner()] += withdrawalFeeAmount;
            }
        }
        return true;
    }

    /**
     * @dev End the auction and send shares of highest bid to the beneficiaries.
     */
    function auctionEnd(uint256 _tokenId) public {
        // Check that auction time is over
        require(
            block.timestamp >= auctionEndTime[_tokenId],
            'ERC721Auctionable: Auction is not yet ended.'
        );
        require(
            auctionState[_tokenId] == AuctionState.Started,
            'ERC721Auctionable: auctionEnd has already been called.'
        );

        // Update auction state (lock second call of the function)
        auctionState[_tokenId] = AuctionState.Ended;

        // Define new owner
        address newOwner = highestBidder[_tokenId];
        address oldOwner = ownerOf(_tokenId);
        uint256 totalAmount = highestBid[_tokenId];

        // Fire event that auction is ended
        emit AuctionEnded(_tokenId, newOwner, totalAmount);

        // Set auction in 'ready to start' state
        auctionState[_tokenId] = AuctionState.ReadyToStart;

        // If newOwner is not zero address then transfer token and make payments
        if (newOwner != address(0)) {
            // Transfer token to new owner
            _transfer(oldOwner, newOwner, _tokenId);

            // Start compute platform fee from pendingReturns of owner
            uint256 platformAmount = pendingReturns[owner()];
            pendingReturns[owner()] = 0;

            // Compute payouts

            // First compute author and gallerist payouts (authorFee and galleristFee of delta if previous highest price is reached)
            uint256 authorAmount = 0;
            uint256 galleristAmount = 0;
            if (totalAmount > tokenHighestPrice[_tokenId]) {
                uint256 delta = totalAmount - tokenHighestPrice[_tokenId];
                authorAmount = (delta * authorFee) / 1000000;
                galleristAmount = (delta * galleristFee) / 1000000;
                // Update token highest price
                tokenHighestPrice[_tokenId] = totalAmount;
            }

            // Beneficiary takes beneficiaryShare of (highestBid - authorFee)
            uint256 beneficiaryAmount = (totalAmount * beneficiaryShare[_tokenId]) / 1000000;
            // The platform takes firstSellFee from first sell for the token
            uint256 sellFeeAmount = 0;
            if (!notFirstSell[_tokenId]) {
                sellFeeAmount = (totalAmount * firstSellFee) / 1000000;
                notFirstSell[_tokenId] = true;
            } else {
                sellFeeAmount = (totalAmount * secondarySellFee) / 1000000;
            }

            // Old owner takes the rest
            uint256 ownerAmount =
                totalAmount - beneficiaryAmount - sellFeeAmount - galleristAmount - authorAmount;
            // Try to send ether to the author. If failed then transfer author fee to platform
            if (authorAmount > 0 && !payable(author[_tokenId]).send(authorAmount)) {
                platformAmount += authorAmount;
            }
            // Try to send ether to the beneficiary.
            // If failed we use platform account.
            if (!payable(beneficiary[_tokenId]).send(beneficiaryAmount)) {
                platformAmount += beneficiaryAmount;
            }
            // Try to send ether to the gallerist.
            // If gallerist is zero address we send ether to the platform account.
            // If failed we use platform account.
            if (highestGallerist[_tokenId] != address(0)) {
                if (!payable(highestGallerist[_tokenId]).send(galleristAmount)) {
                    platformAmount += galleristAmount;
                }
            } else {
                platformAmount += galleristAmount;
            }

            // Try to send ether to the old owner.
            // If failed we use platform account.
            if (ownerAmount > 0 && !payable(oldOwner).send(ownerAmount)) {
                platformAmount += ownerAmount;
            }

            // Add firstSellFeeAmount and send common platform fee
            platformAmount += sellFeeAmount;
            if (!platformAccount.send(platformAmount)) {
                // If sending failed then count platform amount using pendingReturns
                pendingReturns[owner()] = platformAmount;
            }
        }
    }

    /**
     * @dev Make offer to buy the token. Message value must be (offerFee * price / 1 000 000) higher than price
     */
    function makeOffer(
        uint256 _tokenId,
        address _gallerist,
        uint256 _price
    ) public payable {
        // Only when auction is not running
        require(
            auctionState[_tokenId] == AuctionState.ReadyToStart,
            'ERC721Auctionable: auction has to be off.'
        );

        // Check that auction creation is allowed
        require(!isPaused);

        // Check that msg.value is high enough
        uint256 priceWithFee = _price + (_price * offerFee) / 1000000;
        if (msg.value < priceWithFee) {
            require(
                priceWithFee - msg.value <= pendingReturns[msg.sender],
                'ERC721Auctionable: Not enough ether to make offer'
            );

            // If ether value is less than offer price with fee then we take lacking part from pendingReturns
            pendingReturns[msg.sender] -= priceWithFee - msg.value;
        }

        // If sended amount was higher than priceWithFee then increase pendingReturns
        if (msg.value > priceWithFee) {
            pendingReturns[msg.sender] += msg.value - priceWithFee;
        }

        // Take offer fee and save offer price
        pendingReturns[owner()] += priceWithFee - _price;
        offers[_tokenId][msg.sender] = _price;
        offersGallerists[_tokenId][msg.sender] = _gallerist;

        // Fire the event
        emit NewPriceOffer(_tokenId, msg.sender, _price);
    }

    /**
     * @dev Revoke offer to buy the token
     */
    function revokeOffer(uint256 _tokenId) public {
        // Offer price must be positive value to revoke
        require(offers[_tokenId][msg.sender] > 0);

        // Update pendingReturns and offers
        uint256 price = offers[_tokenId][msg.sender];
        uint256 priceWithFee = price + (price * offerFee) / 1000000;
        pendingReturns[msg.sender] += priceWithFee;
        offers[_tokenId][msg.sender] = 0;

        // Fire the event
        emit NewPriceOffer(_tokenId, msg.sender, 0);
    }

    /**
     * @dev Accept offer to buy the token
     */
    function acceptOffer(uint256 _tokenId, address _offerer) public {
        // Check call permission
        require(
            _isApprovedOrOwner(_msgSender(), _tokenId),
            'ERC721Auctionable: acceptOffer caller is not owner nor approved'
        );

        // Only when auction is not running
        require(
            auctionState[_tokenId] == AuctionState.ReadyToStart,
            'ERC721Auctionable: auction has to be off.'
        );

        // Offer price must be positive value
        uint256 price = offers[_tokenId][_offerer];
        require(price > 0, 'Offer price should be a positive number');

        // First reset offer
        offers[_tokenId][_offerer] = 0;

        // Сompute author payout (authorFee of delta if previous highest price is reached)
        uint256 authorAmount = 0;
        address authorAddress = author[_tokenId];
        uint256 galleristAmount = 0;

        if (price > tokenHighestPrice[_tokenId]) {
            uint256 delta = price - tokenHighestPrice[_tokenId];
            authorAmount = (delta * authorFee) / 1000000;
            galleristAmount = (delta * galleristFee) / 1000000;
            // Update token highest price
            tokenHighestPrice[_tokenId] = price;
        }

        // The platform takes firstSellFee from first sell for the token
        uint256 sellFeeAmount = 0;
        if (!notFirstSell[_tokenId]) {
            sellFeeAmount = (price * firstSellFee) / 1000000;
            notFirstSell[_tokenId] = true;
        } else {
            sellFeeAmount = (price * secondarySellFee) / 1000000;
        }

        // Compute old owner payout
        uint256 oldOwnerAmount = price - authorAmount - galleristAmount - sellFeeAmount;
        address oldOwner = ownerOf(_tokenId);

        // Transfer token to new owner
        _transfer(oldOwner, _offerer, _tokenId);

        // Try to send ether to the author and old owner.
        // If failed then count fee using pendingReturns
        if (authorAmount > 0 && !payable(authorAddress).send(authorAmount)) {
            pendingReturns[authorAddress] += authorAmount;
        }
        if (!payable(oldOwner).send(oldOwnerAmount)) {
            pendingReturns[oldOwner] += oldOwnerAmount;
        }

        // Try to send ether to the gallerist.
        // If gallerist is zero address we send ether to the platform account.
        // If failed we use platform account.
        uint256 platformAmount = 0;
        address galleristAddress = offersGallerists[_tokenId][_offerer];

        if (galleristAddress != address(0)) {
            if (!payable(galleristAddress).send(galleristAmount)) {
                platformAmount += galleristAmount;
            }
        } else {
            platformAmount += galleristAmount;
        }

        // Platform fee
        pendingReturns[owner()] += platformAmount + sellFeeAmount;

        // Fire the event
        emit OfferAccepted(_tokenId, _offerer, price);
    }

    /**
     * @dev Set price (in Wei) for the token. It should be a positive number.
     * Only owner of token can set price if auction is not running
     */
    function setPrice(uint256 _tokenId, uint256 _price) public {
        require(
            msg.sender == ownerOf(_tokenId),
            'ERC721Auctionable: only token owner can set price.'
        );
        require(_price > 0, 'ERC721Auctionable: price must be positive value.');
        require(
            auctionState[_tokenId] == AuctionState.ReadyToStart,
            'ERC721Auctionable: auction has to be off.'
        );

        // Update price
        uint256 oldPrice = prices[_tokenId];
        prices[_tokenId] = _price;
        emit NewPrice(_tokenId, oldPrice, _price);
    }

    /**
     * @dev Set price to zero for the token if it has some positive price.
     * Only owner can do it.
     */
    function resetPrice(uint256 _tokenId) public {
        require(
            msg.sender == ownerOf(_tokenId),
            'ERC721Auctionable: only token owner can reset price.'
        );
        require(prices[_tokenId] > 0, 'ERC721Auctionable: price is already zero.');

        // Update price
        uint256 oldPrice = prices[_tokenId];
        prices[_tokenId] = 0;
        emit NewPrice(_tokenId, oldPrice, 0);
    }

    /**
     * @dev Transfer token to new owner if msg.value with pendingReturns is higher or equals the token price.
     * User can call the method only when auction is not running
     */
    function buyToken(uint256 _tokenId, address _gallerist) public payable {
        require(prices[_tokenId] > 0, 'ERC721Tradable: Price must be a positive value');
        require(
            auctionState[_tokenId] == AuctionState.ReadyToStart,
            'ERC721Auctionable: auction has to be off.'
        );

        // Check that auction creation is allowed
        require(!isPaused);

        // Check that message value is enough high
        uint256 price = prices[_tokenId];
        uint256 priceWithFee = price + (price * saleFee) / 1000000;
        if (msg.value < priceWithFee) {
            require(
                priceWithFee - msg.value <= pendingReturns[msg.sender],
                'ERC721Auctionable: Not enough ether to buy token.'
            );

            // If ether value is less than price with fee then we take lacking part from pendingReturns
            pendingReturns[msg.sender] -= priceWithFee - msg.value;
        }

        // If sended amount was higher than priceWithFee then increase pendingReturns
        if (msg.value > priceWithFee) {
            pendingReturns[msg.sender] += msg.value - priceWithFee;
        }

        // Reset token price
        prices[_tokenId] = 0;

        // Сompute author payout (authorFee of delta if previous highest price is reached)
        uint256 authorAmount = 0;
        address authorAddress = author[_tokenId];
        uint256 galleristAmount = 0;
        if (price > tokenHighestPrice[_tokenId]) {
            uint256 delta = price - tokenHighestPrice[_tokenId];
            authorAmount = (delta * authorFee) / 1000000;
            galleristAmount = (delta * galleristFee) / 1000000;
            // Update token highest price
            tokenHighestPrice[_tokenId] = price;
        }

        // Take sale fee
        // The platform takes firstSellFee from first sell for the token
        uint256 sellFeeAmount = 0;
        if (!notFirstSell[_tokenId]) {
            sellFeeAmount = (price * firstSellFee) / 1000000;
            notFirstSell[_tokenId] = true;
        } else {
            sellFeeAmount = (price * secondarySellFee) / 1000000;
        }

        // Compute old owner payout
        address oldOwner = ownerOf(_tokenId);
        uint256 oldOwnerAmount = price - authorAmount - sellFeeAmount;

        // Transfer token ownership
        _transfer(oldOwner, msg.sender, _tokenId);

        // Try to send ether to the author and old owner.
        // If failed then count fee using pendingReturns
        if (authorAmount > 0 && !payable(authorAddress).send(authorAmount)) {
            pendingReturns[authorAddress] += authorAmount;
        }
        if (!payable(oldOwner).send(oldOwnerAmount)) {
            pendingReturns[oldOwner] += oldOwnerAmount;
        }

        // Try to send ether to the gallerist.
        // If gallerist is zero address we send ether to the platform account.
        // If failed we use platform account.
        uint256 platformAmount = 0;
        address galleristAddress = _gallerist;

        if (galleristAddress != address(0)) {
            if (!payable(galleristAddress).send(galleristAmount)) {
                platformAmount += galleristAmount;
            }
        } else {
            platformAmount += galleristAmount;
        }

        // Platform fee
        pendingReturns[owner()] += sellFeeAmount + priceWithFee + platformAmount - price;
    }

    /**
     * @dev If action is not ended for the token then transfer is not allowed.
     * It called before token transfer, so we reset token price
     */
    function isTransferAllowed(uint256 _tokenId) internal override returns (bool) {
        bool isAllowed = auctionState[_tokenId] == AuctionState.ReadyToStart;
        if (isAllowed) prices[_tokenId] = 0;
        return isAllowed;
    }

    /**
     * @dev ...
     * @return ...
     */
    function baseTokenURI() public pure virtual returns (string memory);

    /**
     * @dev ...
     * @return ...
     */
    function tokenURI(uint256 _tokenId) public pure override returns (string memory) {
        return string(abi.encodePacked(baseTokenURI(), Strings.toString(_tokenId)));
    }
}
